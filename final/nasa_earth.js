var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=YEAR-MONTH-DAY";
var myApiKey = "nl4cCjPjgyfewWBiSoPNC5rnzRFunnl5YQqH6MeR";

var example_image_url = "https://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20150613110250_01.png?api_key=";

var epic_natural_archive_base = "https://api.nasa.gov/EPIC/archive/natural/";
var api_url_query_base = "https://epic.gsfc.nasa.gov/api/natural/date/";

// var search_url = "https://epic.gsfc.nasa.gov/api/natural/date/YYYY-MM-DD?api_key=";


// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {

  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);

  // process the "future" img element dynamically generated
  $("div").on("click", "img", function(){
    console.log(this.src);
    // call render_highres() and display the high resolution
  });

  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

    // get the value of the input search text box => date
    var date = $('#search_date').val();
    // console.log(date);
    // build an info object to hold the search term and API key
    var info = {};
    var date_array = date.split('-');
    console.log("date_array: " + date_array);
    info.year = date_array[0];
    info.month = date_array[1];
    info.day = date_array[2];

    console.log("info.year:" + info.year + " info.month:" + info.month + " info.date:" + info.day);
    var infoDate = info.year + "-" + info.month + "-" + info.day + "?";
    var infoDate_v2 = info.year + "/" + info.month + "/" + info.day + "/";
    console.log("InfoDate: " + infoDate);
    // info.api_key =

    // build the search url and sling it into the URL request HTML element
    // var search_url = epic_natural_archive_base + infoDate + "png/" + epic_1b_20170811010437 + ".png?api_key=" +myApiKey;
    var search_url = "https://epic.gsfc.nasa.gov/api/natural/date/" + infoDate + "api_key=" + myApiKey;
    console.log(search_url);
    $('#reqURL').empty();
    $('#reqURL').append(search_url);
    // sling it!
    // make the jQuery AJAX call!
    $.ajax({
      url: search_url,
      success: function(data) {
        render_images(data,info);
      },
      cache: false
    });
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data,info) {
    // get NASA earth data from search results data
    console.log(data.length);
    var images = [];
    for (var i = 0; i < data.length; i++) {
      // build an array of objects that captures all of the key image data
      // => image url
      var img_get = data[i]['image']
      console.log("img_get: " + img_get)


      var img_URL = "https://api.nasa.gov/EPIC/archive/natural/" + info.year + "/" + info.month + "/" + info.day + "/png/" + data[i]['image'] + ".png?api_key=" + myApiKey;
      console.log("img-URL: " + img_URL);

      // => centroid coordinates to be displayed in the caption area
      var coor_lat = data[i]['centroid_coordinates']['lat'];
      console.log("latitude: " + coor_lat);

      var coor_lon = data[i]['centroid_coordinates']['lon'];
      console.log("longitude: " + coor_lon);
      // console.log("coordinate: " + coordinate_points)
      // => image date to be displayed in the caption area (under thumbnail)
      var img_date = data[i]['date'];
      console.log(img_date);

      var allInfo = {};

    }

    var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img src=" + img_URL[i] + "></div><div class='image-caption'></div><div class='image-coords'>" + coor_lat[i] + ", " + coor_lon[i] + "</div></div>";
    $('#image-grid').append(image_cell_template);

    // select the image grid and clear out the previous render (if any)
    var earth_dom = $('#image-grid');
    earth_dom.empty();

    // render all images in an iterative loop here!
    return img_get;
  }

  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
  function render_highres(src_url) {
    // use jQuery to select and maniupate the portion of the DOM => #image-grid
    //  to insert your high resolution image
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});
